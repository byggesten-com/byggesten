// If you like Byggesten, please don't remove this so we can get even better
document.insertBefore(document.createComment(' We using byggesten.com '), document.firstChild);

// ----------

localStorage.setItem('b-select-wrapper', 'b-select-wrapper');
localStorage.setItem('b-select-item', 'b-select-item');
localStorage.setItem('b-search-item', 'b-search-item');
localStorage.setItem('b-search-content', 'b-search-content');
localStorage.setItem('b-required-for', 'b-required-for');
localStorage.setItem('b-tab-content', 'b-tab-content');
localStorage.setItem('b-tab-switch', 'b-tab-switch');
localStorage.setItem('b-step-back', 'b-step-back');
localStorage.setItem('b-step-next', 'b-step-next');
localStorage.setItem('b-modal', 'b-modal');
localStorage.setItem('b-nav', 'b-nav');
localStorage.setItem('b-nav-top', 'b-nav-top');
localStorage.setItem('b-nav-sub', 'b-nav-sub');
localStorage.setItem('b-table-wrapper', 'b-table-wrapper');
localStorage.setItem('b-table-tbody', 'b-table-tbody');
localStorage.setItem('b-table-search', 'b-table-search');
localStorage.setItem('b-table-limit', 'b-table-limit');
localStorage.setItem('b-table-filter', 'b-table-filter');
localStorage.setItem('b-table-paginator-first', 'b-table-paginator-first');
localStorage.setItem('b-table-paginator-back', 'b-table-paginator-back');
localStorage.setItem('b-table-paginator-current', 'b-table-paginator-current');
localStorage.setItem('b-table-paginator-counter', 'b-table-paginator-counter');
localStorage.setItem('b-table-paginator-total', 'b-table-paginator-total');
localStorage.setItem('b-table-paginator-next', 'b-table-paginator-next');
localStorage.setItem('b-table-paginator-last', 'b-table-paginator-last');
localStorage.setItem('b-table-select-all', 'b-table-select-all');
localStorage.setItem('b-table-select-all-content', 'b-table-select-all-content');
localStorage.setItem('b-table-sort-active', 'b-table-sort-active');
localStorage.setItem('b-table-sort-reverse', 'b-table-sort-reverse');
localStorage.setItem('b-table-sort-attr', 'b-table-sort-attr');

// ----------

String.prototype.ltrim = function(text) {
	return this.replace(new RegExp('^(' + (text == undefined ? '\\s' : text) + ')*'), '');
};

String.prototype.rtrim = function(text) {
	return this.replace(new RegExp('(' + (text == undefined ? '\\s' : text) + ')*$'), '');
};

// ----------

$.fn.swap = function(to) {
	return this.each(function() {
		if (!to.length)
			return;

		copyTo = to.clone(true);
		copyFrom = $(this).clone(true);
		to.replaceWith(copyFrom);
		$(this).replaceWith(copyTo);
	});
};

$.fn.toggleAttr = function(attr, value1 = null, value2 = null) {
	if (value1 == null && value2 == null)
		this.each(function() {
			if ($(this).is('[' + attr + ']'))
				$(this).removeAttr(attr)
			else
				$(this).attr(attr, '');
		});
	else
		this.each(function() {
			$(this).attr(attr, $(this).attr(attr) == value1 ? value2 : value1);
		});

	return this;
};

$.fn.renameAttr = function(nameOld, nameNew, removeData) {
	return this.each(function() {
		$.attr(this, nameNew, $.attr(this, nameOld));
		$.removeAttr(this, nameOld);

		if (removeData !== false)
			$.removeData(this, nameOld.replace('data-', ''));
	});

	return this;
};

$.fn.hasAttr = function(name) {
	return this.attr(name) !== undefined;
};

$.fn.toggleProp = function(property) {
    $(this).prop(property, (!$(this).prop(property) || ($(this).prop(property) == 'inherit') || ($(this).prop(property) == 'false') || (typeof $(this).prop(property) == 'undefined')) ?? true);
	return this;
};

$.fn.toggleText = function(text1, text2) {
	$(this).text($(this).text() == text1 ? text2 : text1);
	return this;
};

// ----------

var ini = {
	FILTER: function (element, compareAttr, compareData, compareMethod = null) {
		if (compareData == 'reset') {
			$('#' + element + ' > *').removeAttr('filter-only-child');
			return $('#' + element + ' [' + compareAttr + ']').removeAttr('hidden-filter').removeAttr('filter-first-record').removeAttr('filter-last-record');
		}

		$('#' + element + ' [' + compareAttr + ']').each(function() {
			if (compareMethod == '>')
				if ($(this).attr(compareAttr) < compareData)
					$(this).attr('hidden-filter', '');

			if (compareMethod == '<')
				if ($(this).attr(compareAttr) > compareData)
					$(this).attr('hidden-filter', '');
		});
		$('#' + element + ' [' + compareAttr + ']:not([hidden-filter])').first().attr('filter-first-record', '');
		$('#' + element + ' [' + compareAttr + ']:not([hidden-filter])').last().attr('filter-last-record', '');
		
		if ($('#' + element + ' > *:not([hidden-filter])').length == 1)
			$('#' + element + ' > *:not([hidden-filter])').attr('filter-only-child', '');
	},

	PAGINATOR: function (element, input1 = null, input2 = null) {
		function processPaginator(element, action = null) {
			$('#' + element + ' > *:not(:first-of-type):not(hidden)').attr('hidden-paginator', '').removeAttr('paginator-first-record').removeAttr('paginator-last-record');
			page = $('#' + element).attr('paginator-page');
			$('#' + element).parents('[' + localStorage.getItem('b-table-wrapper') + ']').find('[' + localStorage.getItem('b-table-paginator-counter') + ']').text(page);
			records = $('#' + element).attr('paginator-records');
			elements = $('#' + element + ' > *:not(:first-of-type):not([hidden-filter]):not(hidden)');

			total = Math.ceil(elements.length / records);
			$('#' + element).parents('[' + localStorage.getItem('b-table-wrapper') + ']').find('[' + localStorage.getItem('b-table-paginator-total') + ']').text(total);
			$('#' + element).attr('paginator-total', total);
			
			if (total == 0) {
				$('#' + element).parents('[' + localStorage.getItem('b-table-wrapper') + ']').find('[' + localStorage.getItem('b-table-paginator-counter') + ']').text(0);
				$('#' + element).parents('[' + localStorage.getItem('b-table-wrapper') + ']').find('[' + localStorage.getItem('b-table-paginator-first') + '], [' + localStorage.getItem('b-table-paginator-back') + '], [' + localStorage.getItem('b-table-paginator-next') + '], [' + localStorage.getItem('b-table-paginator-last') + '], [' + localStorage.getItem('b-table-paginator-current') + ']').attr('disabled', '');
			} else {
				$('#' + element).parents('[' + localStorage.getItem('b-table-wrapper') + ']').find('[' + localStorage.getItem('b-table-paginator-counter') + ']').text(page);
				$('#' + element).parents('[' + localStorage.getItem('b-table-wrapper') + ']').find('[' + localStorage.getItem('b-table-paginator-first') + '], [' + localStorage.getItem('b-table-paginator-back') + '], [' + localStorage.getItem('b-table-paginator-next') + '], [' + localStorage.getItem('b-table-paginator-last') + '], [' + localStorage.getItem('b-table-paginator-current') + ']').removeAttr('disabled');
			}

			$('#' + element).parents('[' + localStorage.getItem('b-table-wrapper') + ']')

			elements = elements.slice((page * records) - records, page * records);

			elements.first().attr('paginator-first-record', '');
			elements.last().attr('paginator-last-record', '');

			if (elements.length === 0) {
				if (action == 'next') {
					backPaginator(element)
				} else if (action == 'back') {
					nextPaginator(element);
				}
			} else {
				elements.removeAttr('hidden-paginator');
			}
		}
		
		function nextPaginator(element) {
			if ($('#' + element + ' > *:not(:first-of-type)').length <= 1)
				return;

			page = Number($('#' + element).attr('paginator-page'));
			$('#' + element).attr('paginator-page', page + 1);
			processPaginator(element, 'next');
		}
		
		function backPaginator(element) {
			if ($('#' + element + ' > *:not(:first-of-type)').length <= 1)
				return;

			page = Number($('#' + element).attr('paginator-page'));
			$('#' + element).attr('paginator-page', page - 1);
			processPaginator(element, 'back');
		}
		
		if (Number.isInteger(input1) && Number.isInteger(input2)) {
			$('#' + element).attr('paginator-page', input1).attr('paginator-records', input2);
			processPaginator(element);
		}

		if (input1 == 'next')
			nextPaginator(element);

		if (input1 == 'back')
			backPaginator(element);

		if (input1 == 'first') {
			records = $('#' + element).attr('paginator-records');
			$('#' + element).attr('paginator-page', 1).attr('paginator-records', records);
			processPaginator(element);
		}

		if (input1 == 'last') {
			records = $('#' + element).attr('paginator-records');
			total = Math.ceil($('#' + element + ' > *:not(:first-of-type):not([hidden-filter]):not(hidden)').length / records);
			$('#' + element).attr('paginator-page', total).attr('paginator-records', records);
			processPaginator(element);
		}
	},

	SELECT: function (element) {
		$(document).on('click', '#' + element + '[' + localStorage.getItem('b-select-wrapper') + '][single] [' + localStorage.getItem('b-select-item') + ']', function() {
			$(this).parent('[' + localStorage.getItem('b-select-wrapper') + '][single]').find('[' + localStorage.getItem('b-select-item') + ']').not($(this)).removeAttr('active');
			$(this).toggleAttr('active').trigger('change');
		});

		$(document).on('click', '#' + element + '[' + localStorage.getItem('b-select-wrapper') + ']:not([single]) [' + localStorage.getItem('b-select-item') + ']', function() {
			$(this).toggleAttr('active').trigger('change');
		});
	},

	SEARCH: function (element, callback = null) {
		$(document).on('change keypress keyup', '#' + element, function() {
			$('hidden').unwrap('hidden');

			if ($(this).val() == '') {
				$('hidden').children().unwrap('hidden');
				$('[hidden-search][' + localStorage.getItem('b-search-item') + '*=\'' + element + '\']').removeAttr('hidden-search');
				callback();
				return;
			}

			$('[' + localStorage.getItem('b-search-item') + '*=\'' + element + '\']:not([hidden-search])').attr('hidden-search', '').wrap('<hidden>');
			$('[hidden-search][' + localStorage.getItem('b-search-item') + '*=\'' + element + '\'][' + localStorage.getItem('b-search-content') + '*=\'' + $(this).val().toLowerCase() + '\']').removeAttr('hidden-search').unwrap('hidden');
			
			callback();
		});
	},

	REQUIRED: function(element) {
		$('[' + localStorage.getItem('b-required-for') + ']').each(function() {
			$('#' + $(this).attr(localStorage.getItem('b-required-for'))).attr('disabled', '');
		});

		$(document).on('change keypress keyup', '[' + localStorage.getItem('b-required-for') + ']', function() {
			if ($('[' + localStorage.getItem('b-required-for') + '=\'' + element + '\']').filter(function() { return !this.value }).length == 0)
				$('#' + element).removeAttr('disabled');
			else
				$('#' + element).attr('disabled', '');
		});
	},

	SORT: function(element, attr, option) {
		$('#' + element + ' > [' + attr + ']').sort(function(a, b) {
			if ((option == 'alphabet') || (option == 'alphabet-reverse'))
	            if ($(a).attr(attr) > $(b).attr(attr))
	                return option == 'alphabet' ? 1 : -1;
	            else if ($(a).attr(attr) == $(b).attr(attr))
	                return 0;
	            else
	                return option == 'alphabet' ? -1 : 1;

			if ((option == 'number') || (option == 'nearest'))
	            return $(b).attr(attr) - $(a).attr(attr);

			if ((option == 'number-reverse') || (option == 'nearest-reverse'))
				return $(a).attr(attr) - $(b).attr(attr);
        }).each(function() {
            var elem = $(this);
            elem.remove();
            $(elem).appendTo('#' + element);
        });
	},

	TAB: function(opened) {
		$('[' + localStorage.getItem('b-tab-content') + '=\'' + opened + '\']').parent().find('[' + localStorage.getItem('b-tab-content') + ']').attr('hidden', '').end().end().removeAttr('hidden');
		$('[' + localStorage.getItem('b-tab-switch') + '=\'' + opened + '\']').parent().find('[' + localStorage.getItem('b-tab-switch') + ']').removeAttr('active').end().end().attr('active', '');

		$(document).on('click', '[' + localStorage.getItem('b-tab-switch') + ']', function() {
			$('[' + localStorage.getItem('b-tab-content') + '=\'' + $(this).attr(localStorage.getItem('b-tab-switch')) + '\']').parent().find('[' + localStorage.getItem('b-tab-content') + ']').attr('hidden', '').end().end().removeAttr('hidden');
			$(this).parent().find('[' + localStorage.getItem('b-tab-switch') + ']').removeAttr('active').end().end().attr('active', '');
			$('[' + localStorage.getItem('b-tab-switch') + '=\'' + $(this).attr(localStorage.getItem('b-tab-switch')) + '\']').parent().find('[' + localStorage.getItem('b-tab-switch') + ']').removeAttr('active').end().end().attr('active', '');
		});
	},

	VALIDATE: function(elements, regexp, callback) {
		elements.each(function() {
			$(this).on('change', function() {
				if (!(new RegExp(regexp)).test($(this).val()) && ($(this).val().trim() != '')) {
					callback();
					$(this).val('');
				}
			});
		});
	},

	TEXTBEFORE: function(elements, text) {
		elements.each(function() {
			$(this).on('change', function() {
				if ($(this).val().trim() != '')
					$(this).val(text + $(this).val().replace(text, ''));
			});
		});
	},

	TEXTAFTER: function(elements, text) {
		elements.each(function() {
			$(this).on('change', function() {
				if ($(this).val().trim() != '')
					$(this).val($(this).val().replace(text, '') + text);
			});
		});
	},

	STEPS: function(group, last, single) {
		$('[' + group + ']').attr('hidden', '');
		$('[' + group + '=1]').removeAttr('hidden');
		$('[' + localStorage.getItem('b-step-back') + '=' + group + ']').attr('disabled', '');
		
		$(document).on('click', '[' + localStorage.getItem('b-step-back') + '=' + group + ']', function() {
			$('[' + localStorage.getItem('b-step-next') + '=' + group + ']').removeAttr('disabled');

			current = parseInt($('[' + group + ']:not([hidden])').last().attr(group));

			if (single) {
				if (current != 1)
					$('[' + group + '=' + current + ']').attr('hidden', '');

				if (current == 2)
					$('[' + localStorage.getItem('b-step-back') + '=' + group + ']').attr('disabled', '');
			} else {
				$('[' + group + ']').attr('hidden', '');

				while (current > 1) {
					current--;

					if (current == 1) {
						$('[' + group + '=' + current + ']').removeAttr('hidden');
						$('[' + localStorage.getItem('b-step-back') + '=' + group + ']').attr('disabled', '');
					} else
						$('[' + group + '=' + current + ']').removeAttr('hidden');
						break;
				}
			}
		});

		$(document).on('click', '[' + localStorage.getItem('b-step-next') + '=' + group + ']', function() {
			$('[' + localStorage.getItem('b-step-back') + '=' + group + ']').removeAttr('disabled');

			current = parseInt($('[' + group + ']:not([hidden])').last().attr(group));

			if (single) {
				current++;

				$('[' + group + '=' + current + ']').removeAttr('hidden');

				if (current == last)
					$('[' + localStorage.getItem('b-step-next') + '=' + group + ']').attr('disabled', '');
			} else {
				$('[' + group + ']').attr('hidden', '');

				while (current < last) {
					current++;

					if (current == last) {
						$('[' + group + '=' + current + ']').removeAttr('hidden');
						$('[' + localStorage.getItem('b-step-next') + '=' + group + ']').attr('disabled', '');
					} else {
						$('[' + group + '=' + current + ']').removeAttr('hidden');
						break;
					}
				}
			}
		});
	},

	COPY: function(text) {
		var copy = function(e) {
	        e.preventDefault();
	        if (e.clipboardData)
	            e.clipboardData.setData('text/plain', text);
	        else if (window.clipboardData)
	            window.clipboardData.setData('Text', text);
	    }

	    window.addEventListener('copy', copy);

		var $temp = $('<input>');
		$('body').append($temp);
		$temp.val(text).select();
		document.execCommand('copy');
		$temp.remove();

	    window.removeEventListener('copy', copy);
	},

	OUTSIDE: function(event, target) {
		out = true;
	    for (i = 0; i < target.length; i++)
	        if (event.target == target[i] || target[i].contains(event.target)) {
		        out = false;
				break;
			}
		return out;
	},

	MODAL: function(target, parameters) {
		if (parameters.type == 'img') {
			target.on('click', (event) => {
				if (event.target.tagName.toLowerCase() == 'img') {
					$newTarget = $('body').append('<' + localStorage.getItem('b-modal') + ' ' + parameters.background + '><img src="' + $(event.target).attr('src') + '"></' + localStorage.getItem('b-modal') + '>');

					$(window).on('click', (event) => {
						if (ini.OUTSIDE(event, $newTarget.find('img')))
							$(localStorage.getItem('b-modal')).remove();
					});
				}
			});
		}
	}
};

// ----------

var get = {
	SELECTED: function(element) {
		var selected = [];
		$('#' + element + ' [' + localStorage.getItem('b-select-item') + '][active]').each(function() {
			if ($(this).attr('value') != '')
				selected.push($(this).attr('value'));  
		});
		return selected;
	}
};

// ----------

var send = {
	REQUEST: function(route, data = null, callback = null, type) {
		var x = new XMLHttpRequest();
		x.onreadystatechange = function() {
			result = x.responseText || null;

			if ((x.readyState == 4) && (callback != null))
                if (x.status == 200 || x.status == 201)
                    try {
                        callback(JSON.parse(result), true, x.status);
                    } catch(e) {
                        callback(result, true, x.status); 
                    }
                else
                    try {
                        callback(JSON.parse(result), false, x.status);
                    } catch(e) { 
                        callback(result, false, x.status); 
                    }
		}

		x.open(type, route, true);
		x.send(data ?? '');
	},

	POST: (route, data, callback, type) => send.REQUEST(route, data, callback, 'POST'),
	GET: (route, data, callback, type) => send.REQUEST(route, data, callback, 'GET'),
	PUT: (route, data, callback, type) => send.REQUEST(route, data, callback, 'PUT'),
	CREATE: (route, data, callback, type) => send.REQUEST(route, data, callback, 'CREATE'),
	DELETE: (route, data, callback, type) => send.REQUEST(route, data, callback, 'DELETE')
};

// ----------

var cookie = {
	SET: function(name, value, days = 7) {
		var d = new Date();
		d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
		document.cookie = name + '=' + value + ';' + 'expires=' + d.toUTCString() + ';path=/';
	},

	GET: function(name, value, days) {
		var name = name + '=';
		var ca = document.cookie.split(';');

		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ')
				c = c.substring(1);

			if (c.indexOf(name) == 0)
				return c.substring(name.length, c.length);
		}

		return '';
	}
};

// ----------

$(document).ready(function() {
	$(document).off('click', '[' + localStorage.getItem('b-nav') + '] [' + localStorage.getItem('b-nav-top') + ']').on('click', '[' + localStorage.getItem('b-nav') + '] [' + localStorage.getItem('b-nav-top') + ']', function() {
		display = $('[' + localStorage.getItem('b-nav') + ']').attr(localStorage.getItem('b-nav'));
		$(this).attr(localStorage.getItem('b-nav-top'), $(this).attr(localStorage.getItem('b-nav-top')) == 't' ? 'f' : 't').nextUntil('*:not([' + localStorage.getItem('b-nav-sub') + '])', '[' + localStorage.getItem('b-nav-sub') + ']').css('display', $(this).attr(localStorage.getItem('b-nav-top')) == 't' ? (display || 'block') : 'none');
	});

	// ----------

	$(document).find('[' + localStorage.getItem('b-table-wrapper') + ']').each(function() {
		let wrapperID, tbodyID, searchID, limitID, filterID, 
		wrapperElement, tbodyElement, searchElement, limitElement, filterElement,
		paginatorFirstElement, paginatorBackElement, paginatorCurrentElement,
		paginatorCounterElement, paginatorTotalElement, paginatorNextElement,
		paginatorLastElement, selectAllElement, selectAllContentElement, sortActiveElement,
		sortActive1Element, sortReverseElement, sortReverse1Element = null;

		jQuery.randomID = Math.floor(Math.random() * (6666666666 - 6666666) + 6666666);
		const getRandomID = () => --jQuery.randomID;

		wrapperElement = $(this);
		wrapperID = (wrapperElement.attr('id') && (wrapperElement.attr('id').length > 0)) ? wrapperElement.attr('id') : getRandomID();
		wrapperElement.attr('id', wrapperID);
		
		tbodyElement = $(this).find('[' + localStorage.getItem('b-table-tbody') + ']').first();
		if (tbodyElement.length) {
			tbodyID = (tbodyElement.attr('id') && (tbodyElement.attr('id').length > 0)) ? tbodyElement.attr('id') : getRandomID();
			tbodyElement.attr('id', tbodyID);
		}

		searchElement = $(this).find('[' + localStorage.getItem('b-table-search') + ']').first();
		if (!searchElement.is('input'))
			searchElement = searchElement.find('input').first();

		if (searchElement.length) {
			searchID = (searchElement.attr('id') && (searchElement.attr('id').length > 0)) ? searchElement.attr('id') : getRandomID();
			searchElement.attr('id', searchID);
			
			tbodyElement.find('tr[' + localStorage.getItem('b-search-content') + ']').attr(localStorage.getItem('b-search-item'), searchID);
		}

		limitElement = $(this).find('[' + localStorage.getItem('b-table-limit') + ']').first();
		if (!limitElement.is('select'))
			limitElement = limitElement.find('select').first();

		if (limitElement.length) {
			limitID = (limitElement.attr('id') && (limitElement.attr('id').length > 0)) ? limitElement.attr('id') : getRandomID();
			limitElement.attr('id', limitID);
		}

		filterElement = $(this).find('[' + localStorage.getItem('b-table-filter') + ']').first();
		if (!filterElement.is('select'))
			filterElement = filterElement.find('select').first();

		if (filterElement.length) {
			filterID = (filterElement.attr('id') && (filterElement.attr('id').length > 0)) ? filterElement.attr('id') : getRandomID();
			filterElement.attr('id', filterID);
		}

		paginatorFirstElement = $(this).find('[' + localStorage.getItem('b-table-paginator-first') + ']');
		paginatorBackElement = $(this).find('[' + localStorage.getItem('b-table-paginator-back') + ']');
		paginatorCurrentElement = $(this).find('[' + localStorage.getItem('b-table-paginator-current') + ']');
		paginatorCounterElement = $(this).find('[' + localStorage.getItem('b-table-paginator-counter') + ']');
		paginatorTotalElement = $(this).find('[' + localStorage.getItem('b-table-paginator-total') + ']');
		paginatorNextElement = $(this).find('[' + localStorage.getItem('b-table-paginator-next') + ']');
		paginatorLastElement = $(this).find('[' + localStorage.getItem('b-table-paginator-last') + ']');

		selectAllElement = $(this).find('[' + localStorage.getItem('b-table-select-all') + ']');
		if (!selectAllElement.is('input'))
			selectAllElement = selectAllElement.find('input');

		selectAllContentElement = $(this).find('[' + localStorage.getItem('b-table-select-all-content') + ']').attr('hidden', true);

		sortActiveElement = $(this).find('[' + localStorage.getItem('b-table-sort-active') + ']');
		sortActive1Element = $(this).find('[' + localStorage.getItem('b-table-sort-active') + '=1]');
		sortReverseElement = $(this).find('[' + localStorage.getItem('b-table-sort-reverse') + ']');
		sortReverse1Element = $(this).find('[' + localStorage.getItem('b-table-sort-reverse') + '=1]');

		function restore(type = null) {
			if (type == 'sort') {
				ini.PAGINATOR(tbodyID, 1, Number(tbodyElement.attr('paginator-records')));
				filterElement.find('option[selected]').trigger('change');
				checkboxesEventAssign();
			}

			if (type == 'filter')
				ini.PAGINATOR(tbodyID, 1, Number(tbodyElement.attr('paginator-records')));

			if (type == 'search')
				ini.PAGINATOR(tbodyID, 1, Number(tbodyElement.attr('paginator-records')));

			selectAllElement.prop('checked', false).trigger('change');
			tbodyElement.find('th:first-of-type input[type=checkbox], td:first-of-type input[type=checkbox]').prop('checked', false).trigger('change');
		}

		if (sortActive1Element.length)
			sortHandler(sortActive1Element, false);
		else if (sortReverse1Element.length)
			sortHandler(sortReverse1Element, true);

		sortActiveElement.off('click').on('click', function() {
			sortHandler(this, $(this).attr(localStorage.getItem('b-table-sort-active')) == 1);
		});

		function sortHandler(element, reverse) {
			attrName = $(element).attr(localStorage.getItem('b-table-sort-attr'));
			attrValue1 = tbodyElement.find('[' + attrName + ']:nth-of-type(1)').attr(attrName);
			attrValue2 = tbodyElement.find('[' + attrName + ']:nth-of-type(2)').attr(attrName);

			number = false;
			if (!isNaN(attrValue1) && !isNaN(attrValue2))
				number = true;

			sortActiveElement.attr(localStorage.getItem('b-table-sort-active'), 0).attr(localStorage.getItem('b-table-sort-reverse'), 0);

			if (reverse) {
				$(element).attr(localStorage.getItem('b-table-sort-reverse'), 1);
				ini.SORT(tbodyID, $(element).attr(localStorage.getItem('b-table-sort-attr')), number ? 'number-reverse' : 'alphabet-reverse');
			} else {
				$(element).attr(localStorage.getItem('b-table-sort-active'), 1);
				ini.SORT(tbodyID, $(element).attr(localStorage.getItem('b-table-sort-attr')), number ? 'number' : 'alphabet');
			}

			restore('sort');
		}

		selectAllElement.off('click').on('click', function() {
			tbodyElement.find('tr:not([hidden]):not([hidden-temp]):not([hidden-filter]):not([hidden-paginator]) > td:first-of-type input[type=checkbox]').prop('checked', $(this).prop('checked')).trigger('change');
		});

		checkboxesEventAssign();
		function checkboxesEventAssign() {
			tbodyElement.find('td:first-of-type input[type=checkbox]').off('click').on('click', function() {
				// $(this).find('input[type=checkbox]').toggleProp('checked').trigger('change');
				selectAllElement.prop('checked', false).trigger('change');

				checked = false;
				tbodyElement.find('tr:not([hidden]):not([hidden-temp]):not([hidden-filter]):not([hidden-paginator]) > td:first-of-type input[type=checkbox]').each(function() {
					if ($(this).prop('checked') === true) {
						checked = true;
						return;
					}
				});

				selectAllElement.prop('checked', checked).trigger('change');
			});
		}

		selectAllElement.off('change').on('change', function() {
			if ($(this).prop('checked') === true)
				selectAllContentElement.removeAttr('hidden');
			else
				selectAllContentElement.attr('hidden', true);
		});

		paginatorFirstElement.off('click').on('click', function() {
			ini.PAGINATOR(tbodyID, 'first');
			restore();
		});

		paginatorLastElement.off('click').on('click', function() {
			ini.PAGINATOR(tbodyID, 'last');
			restore();
		});

		paginatorBackElement.off('click').on('click', function() {
			ini.PAGINATOR(tbodyID, 'back');
			restore();
		});

		paginatorNextElement.off('click').on('click', function() {
			ini.PAGINATOR(tbodyID, 'next');
			restore();
		});

		paginatorCurrentElement.off('click').on('click', function() {
			content = $(this).html();
			$(this).html('').attr('contenteditable', 'true').focus();
			$(this).off('focusout').on('focusout', function() {
				page = parseInt(Number($(this).text()));
				total = Number(tbodyElement.attr('paginator-total'));
				page = isNaN(page) ? 1 : page;
				page = (page < 1) ? 1 : page;
				page = (page > total) ? total : page;
				$(this).removeAttr('contenteditable').html(content).find('[' + localStorage.getItem('b-table-paginator-counter') + ']').text(page).end().find('[' + localStorage.getItem('b-table-paginator-total') + ']').text(total);
				if (page > 0)
					ini.PAGINATOR(tbodyID, page, Number(tbodyElement.attr('paginator-records')));
			});
		});

		filterElement.off('change').on('change', function() {
			if ($(this).val() == 'all')
				ini.FILTER(tbodyID, 'sort-date', 'reset');

			if ($(this).val() == 'today') {
				ini.FILTER(tbodyID, 'sort-date', 'reset');
				ini.FILTER(tbodyID, 'sort-date', moment().format('YYYYMMDD'), '>');
				ini.FILTER(tbodyID, 'sort-date', moment().format('YYYYMMDD'), '<');
			}

			if ($(this).val() == 'yesterday') {
				ini.FILTER(tbodyID, 'sort-date', 'reset');
				ini.FILTER(tbodyID, 'sort-date', moment().subtract(1, 'days').format('YYYYMMDD'), '>');
				ini.FILTER(tbodyID, 'sort-date', moment().subtract(1, 'days').format('YYYYMMDD'), '<');
			}

			if ($(this).val() == 'week') {
				ini.FILTER(tbodyID, 'sort-date', 'reset');
				ini.FILTER(tbodyID, 'sort-date', moment().startOf('isoWeek').format('YYYYMMDD'), '>');
				ini.FILTER(tbodyID, 'sort-date', moment().endOf('isoWeek').format('YYYYMMDD'), '<');
			}

			if ($(this).val() == 'month') {
				ini.FILTER(tbodyID, 'sort-date', 'reset');
				ini.FILTER(tbodyID, 'sort-date', moment().startOf('month').format('YYYYMMDD'), '>');
				ini.FILTER(tbodyID, 'sort-date', moment().endOf('month').format('YYYYMMDD'), '<');
			}

			if ($(this).val() == 'year') {
				ini.FILTER(tbodyID, 'sort-date', 'reset');
				ini.FILTER(tbodyID, 'sort-date', moment().startOf('year').format('YYYYMMDD'), '>');
				ini.FILTER(tbodyID, 'sort-date', moment().endOf('year').format('YYYYMMDD'), '<');
			}

			if ($(this).val() == '7days') {
				ini.FILTER(tbodyID, 'sort-date', 'reset');
				ini.FILTER(tbodyID, 'sort-date', moment().subtract(7, 'days').format('YYYYMMDD'), '>');
				ini.FILTER(tbodyID, 'sort-date', moment().format('YYYYMMDD'), '<');
			}

			if ($(this).val() == '30days') {
				ini.FILTER(tbodyID, 'sort-date', 'reset');
				ini.FILTER(tbodyID, 'sort-date', moment().subtract(30, 'days').format('YYYYMMDD'), '>');
				ini.FILTER(tbodyID, 'sort-date', moment().format('YYYYMMDD'), '<');
			}

			if ($(this).val() == '365days') {
				ini.FILTER(tbodyID, 'sort-date', 'reset');
				ini.FILTER(tbodyID, 'sort-date', moment().subtract(365, 'days').format('YYYYMMDD'), '>');
				ini.FILTER(tbodyID, 'sort-date', moment().format('YYYYMMDD'), '<');
			}

			restore('filter');
		});

		if (limitElement.length) {
			ini.PAGINATOR(tbodyID, 1, Number($('select#' + limitID).val()));

			limitElement.off('change').on('change', function() {
				ini.PAGINATOR(tbodyID, 1, Number($(this).val()));
				restore();
			});
		}

		if (searchElement.length) {
			ini.SEARCH(searchID, () => {
				// restore('search');
				ini.PAGINATOR(tbodyID, 1, Number(tbodyElement.attr('paginator-records')));
				selectAllElement.prop('checked', false).trigger('change');
				tbodyElement.find('th:first-of-type input[type=checkbox], td:first-of-type input[type=checkbox]').prop('checked', false).trigger('change');
			});
		}

		$('<style>').text('[' + localStorage.getItem('b-table-wrapper') + '] tr { display: table-row !important; }').appendTo(document.head);
	});
});