Founded in 2021. Provided under the MIT license. Made with love in Prague by [Strømstik](https://stromstik.com)

**Byggesten x Strømstik — Rendering**\
Rendering core for [Strømstik](https://stromstik.com) written on PHP.

**Byggesten JS**\
JS & jQuery component with usefull tools.

**Byggesten CSS**\
Native CSS and HTML component. Similar to basic Bootstrap, but simplified.

# Byggesten x Strømstik — Rendering
Soon...

# Byggesten JS
### Left & Right Trim
```
<script>
    console.log('---text'.ltrim('-')); // Result: text
    console.log('text---'.rtrim('-')); // Result: text
</script>
```

### Has attribute
```
<div hidden id="element-1"></div>

<script>
	console.log($('#element-1').hasAttr('hidden')); // Result: true
</script>
```

### Toggle attribute
Toggling the attribute itself:
```
<div hidden id="element-1"></div>

<script>
	$('#element-1').toggleAttr('hidden');
</script>
```

Toggling between one value:
```
<div test="1" id="element-1"></div>

<script>
	$('#element-1').toggleAttr('test', '1');
</script>
```

Toggling between two values:
```
<div test="1" id="element-1"></div>

<script>
	$('#element-1').toggleAttr('test', '1', '2');
</script>
```

### Rename attribute
Changing the attribute name:
```
<div disabled id="element-1"></div>

<script>
	$('#element-1').renameAttr('disabled', 'active');
</script>
```

The function provides a third parameter to remove the value set via `.data()`:
```
<div data-test id="element-1"></div>

<script>
	$('#element-1').renameAttr('data-test', 'test', true);
</script>
```

### Toggle property
Toggling of property between `true` and `false`:
```
<input type="checkbox" id="input-1">

<script>
	$('#input-1').toggleProp('checked');
</script>
```

### Toggle text
Toggling of text in element between two values:
```
<div id="element-1">Send</div>

<script>
	$('#element-1').toggleText('Send', 'Edit');
</script>
```

### Swap Elements
```
<div id="element-1">Element 1</div>
<div id="element-2">Element 2</div>

<script>
	$('#element-1').swap($('#element-2'));
</script>
```

### Outside Click
```
<div element>Element 1</div>
<div element>Element 2</div>

<script>
    $(window).on('click', function(event) {
        if (ini.OUTSIDE(event, $('[element]')))
            console.log('outside');
        else
            console.log('inside');
    });
</script>
```

### IMG Modal
```
<img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png" height="100" id="picture-1">

<script>
    ini.MODAL($('#picture-1'), {
        'type': 'img',
        'background': 'white' // or black
    });
</script>
```

### Required Inputs
Allow clicking on a button only if all the required fields are filled in. Add the `b-required-for` attribute with the `ID of button` to the required `input` / `textarea` / `select`. The button, by default, will have a `disabled` attribute.

```
<input type="text" b-required-for="button-1">
<input type="text" b-required-for="button-1">

<button id="button-1">Test</button>

<script>
    ini.REQUIRED('button-1');
</script>
```

### Custom Select
The `b-select-wrapper` attribute is required and denotes a group of elements. The `single` attribute makes it possible to select only one of the elements (you can remove it). The `b-select-item` attribute is required and denotes a group's item. The value of each of the elements must be in the `value` attribute. Attribute `active` is added to selected items.

```
<div b-select-wrapper single id="select-1">
    <div b-select-item value="1">Item 1</div>
    <div b-select-item value="2">Item 2</div>
    <div b-select-item value="3">Item 3</div>
</div>

<script>
    ini.SELECT('select-1');
    console.log(get.SELECTED('select-1')); // Get value of selected item(s)
</script>
```

### Simple Search
To connect an element to a search, use the `b-search-item` attribute with the value of the `ID of search input`. Write content (in lowercase) used for search in the `b-search-content` attribute. The `b-search-content` attribute can be replaced with any other attribute — during initialization add its name as the second parameter. Hidden elements are wrapped with `hidden` element.

```
<input type="text" id="input-1">

<div>
    <div b-search-item="input-1" b-search-content="my test name">My Test Name</div>
    <div b-search-item="input-1" b-search-content="eman tset ym">Eman Tset Ym</div>
    <div b-search-item="input-1" b-search-content="my tset name">My Tset Name</div>
</div>

<script>
    ini.SEARCH('input-1');
</script>
```

### Simple Sorting
Use this simple function to sort all sibling items. Input parameters for initialization: `ID of wrapper`, `attribute with data`, `sorting method`.

```
<div id="wrapper-1">
    <div sort-name="abc" sort-time="1614593151">abc, 1614593151</div>
    <div sort-name="cba" sort-time="1614582044">cba, 1614582044</div>
    <div sort-name="bca" sort-time="1614590000">bca, 1614590000</div>
</div>

<script>
    ini.SORT('wrapper-1', 'sort-name', 'alphabet');
    ini.SORT('wrapper-1', 'sort-name', 'alphabet-reverse');
    ini.SORT('wrapper-1', 'sort-time', 'number');
    ini.SORT('wrapper-1', 'sort-time', 'number-reverse');
</script>
```

### Request Sending
A compact wrapper for native AJAX. Input parameters: `route`, `variable with data`, `callback function`.  
Avalible parameters in `callback function`: `result`, `success (true/false)`, `status code (200, 404 etc.)`.  

An example of a POST request sending data of different types (text, array, files):
```
<script>
    data = new FormData();

    data.append('key', 'value');
    data.append('selected', get.SELECTED('selectID'));

    $.each($('#images')[0].files, (index, file) => data.append('images' + index, file));

    send.POST('/route', data, (result, success, code) => alert(result + ' ' + success + ' ' + code));
</script>
```

Instead of `POST`, you can also use: `GET`, `PUT`, `CREATE`, `DELETE`.

### Simple Tabs
Declare swithces through the `b-tab-switch` attribute and content to switch through the `b-tab-content` attribute. Bind `b-tab-switch` to `b-tab-content` through the same value of their attributes. Logically related `b-tab-content` must have a common parent. Input parameters for initialization: the value of the `b-tab-content` to be displayed initially. 

```
<button b-tab-switch="tab-1">Switch 1</button>
<button b-tab-switch="tab-2">Switch 2</button>

<div b-tab-content="tab-1">Content 1</div>
<div b-tab-content="tab-2">Content 2</div>

<script>
    ini.TAB('tab-1');
</script>
```

### Simple Validation
Input parameters for initialization: `set of elements`, `RegExp`, `callback function`.

```
<input type="text">
<input type="text">

<script>
    ini.VALIDATE($('input'), '^([a-zA-Z0-9-_]+)(.js)$', () => alert('Validation error'));
</script>
```

### Auto before/after text for inputs
```
<input type="text">
<input type="text">

<script>
    ini.TEXTAFTER($('input'), '.js');
    ini.TEXTBEFORE($('input'), 'File: ');
</script>
```

### Simple Validation + Auto before/after text for inputs
```
<input type="text">
<input type="text">

<script>
    ini.TEXTAFTER($('input'), '.js'); // Will be validated
    ini.VALIDATE($('input'), '^([a-zA-Z0-9-_]+)(.js)$', () => alert('Validation error')); // Validation
    ini.TEXTBEFORE($('input'), 'File: '); // Will not be validated
</script>
```

### Simple Steps
Declare step switches using the `b-step-back` and `b-step-next` attributes. Bind the switches using a `unique ID` in the values of their attributes. Declare the content of the steps and associate it with the switches using the previously declared `unique ID` as the name of the attribute itself. The value of these attributes will be their sequential `index`. Logically related steps must have a common parent. Input parameters for initialization: the `unique ID` used for the binding, the `index` of the last step, optional control of `steps extension mode` (replacement by default).

Replacement of steps:
```
<button b-step-back="test">Back</button>
<button b-step-next="test">Next</button>

<div test="1">Content 1</div>
<div test="2">Content 2</div>
<div test="3">Content 3</div>

<script>
    ini.STEPS('test', 3);
</script>
```

Extension of steps:
```
<button b-step-back="test">Back</button>
<button b-step-next="test">Next</button>

<div test="1">Content 1</div>
<div test="2">Content 2</div>
<div test="3">Content 3</div>

<script>
    ini.STEPS('test', 3, true);
</script>
```

### Copy Plugin
Programmatically copying of text with multi-browser support.

```
<button id="button-1">Copy</button>

<script>
    $('#button-1').on('click', () => ini.COPY('test'));
</script>
```

### Cookies Plugin
```
cookie.SET('key', 'value', 365);
cookie.GET('key');
```

### Simple Navigation
Use the key attribute `b-nav` for the container. Highlight the main categories with the `b-nav-top` attribute, sub-categories — with the `b-nav-sub` attribute.

```
<div b-nav>
    <div b-nav-top>Top level record 1</div>
    <div b-nav-sub>Sub level record 1</div>
    <div b-nav-sub>Sub level record 2</div>
    <div b-nav-sub>Sub level record 3</div>
    <div b-nav-top>Top level record 2</div>
    <div b-nav-sub>Sub level record 1</div>
    <div b-nav-top>Top level record 3</div>
    <div b-nav-sub>Sub level record 1</div>
    <div b-nav-sub>Sub level record 2</div>
</div>
```

### Byggesten Tables
Explanation soon.

```
<div b-table-wrapper>
	<input type="text" placeholder="Search" b-table-search>

	<select b-table-limit>
		<option value="2" selected>2 per page</option>
		<option value="3">3 per page</option>
	</select>

	<select b-table-filter>
		<option value="all" selected>All time</option>
		<option value="today">Today</option>
		<option value="yesterday">Yesterday</option>
		<option value="week">This week</option>
		<option value="month">This month</option>
		<option value="year">This year</option>
		<option value="7days">Past 7 d.</option>
		<option value="30days">Past 30 d.</option>
		<option value="365days">Past 365 d.</option>
	</select>

	<div b-table-paginator-first>First</div>
	<div b-table-paginator-back>Back</div>

	<div b-table-paginator-current>
		<span b-table-paginator-counter>1</span>
		<span>of</span>
		<span b-table-paginator-total></span>
	</div>

	<div b-table-paginator-next>Next</div>
	<div b-table-paginator-last>Last</div>

	<div b-table-select-all-content>Here should be bulk controls</div>

	<table>
		<tbody b-table-tbody>
			<tr>
				<th><input type="checkbox" b-table-select-all></th>
				<th b-table-sort-active="1" b-table-sort-reverse="0" b-table-sort-attr="sort-date">Date</th>
				<th b-table-sort-active="0" b-table-sort-reverse="0" b-table-sort-attr="sort-name">Name</th>
			</tr>

			<tr b-search-content="den rodriguez" sort-date="20220109" sort-name="Den Rodriguez">
				<td><input type="checkbox"></td>
				<td>09.01.2022</td>
				<td>Den Rodriguez</td>
			</tr>

			<tr b-search-content="den sanchez" sort-date="20000605" sort-name="Den Sanchez">
				<td><input type="checkbox"></td>
				<td>05.06.2000</td>
				<td>Den Sanchez</td>
			</tr>

			<tr b-search-content="elon musk" sort-date="19710628" sort-name="Elon Musk">
				<td><input type="checkbox"></td>
				<td>28.06.1971</td>
				<td>Elon Musk</td>
			</tr>

			<tr b-search-content="sergei mavrodi" sort-date="19550811" sort-name="Sergei Mavrodi">
				<td><input type="checkbox"></td>
				<td>11.08.1955</td>
				<td>Sergei Mavrodi</td>
			</tr>
		</tbody>
	</table>
</div>
```

### Custom Names
You can change most of the Byggesten's attribute and element names. Replace the second parameter in the `setItem` function with your desired name. Names must not be repeated.

```
localStorage.setItem('b-select-wrapper', 'b-select-wrapper');
localStorage.setItem('b-select-item', 'b-select-item');
localStorage.setItem('b-search-item', 'b-search-item');
localStorage.setItem('b-search-content', 'b-search-content');
localStorage.setItem('b-required-for', 'b-required-for');
localStorage.setItem('b-tab-content', 'b-tab-content');
localStorage.setItem('b-tab-switch', 'b-tab-switch');
localStorage.setItem('b-step-back', 'b-step-back');
localStorage.setItem('b-step-next', 'b-step-next');
localStorage.setItem('b-modal', 'b-modal');
localStorage.setItem('b-nav', 'b-nav');
localStorage.setItem('b-nav-top', 'b-nav-top');
localStorage.setItem('b-nav-sub', 'b-nav-sub');
localStorage.setItem('b-table-wrapper', 'b-table-wrapper');
localStorage.setItem('b-table-search', 'b-table-search');
localStorage.setItem('b-table-limit', 'b-table-limit');
localStorage.setItem('b-table-filter', 'b-table-filter');
localStorage.setItem('b-table-paginator-first', 'b-table-paginator-first');
localStorage.setItem('b-table-paginator-back', 'b-table-paginator-back');
localStorage.setItem('b-table-paginator-current', 'b-table-paginator-current');
localStorage.setItem('b-table-paginator-counter', 'b-table-paginator-counter');
localStorage.setItem('b-table-paginator-total', 'b-table-paginator-total');
localStorage.setItem('b-table-paginator-next', 'b-table-paginator-next');
localStorage.setItem('b-table-paginator-last', 'b-table-paginator-last');
localStorage.setItem('b-table-select-all', 'b-table-select-all');
localStorage.setItem('b-table-select-all-content', 'b-table-select-all-content');
localStorage.setItem('b-table-sort-active', 'b-table-sort-active');
localStorage.setItem('b-table-sort-reverse', 'b-table-sort-reverse');
localStorage.setItem('b-table-sort-attr', 'b-table-sort-attr');
```

# Byggesten CSS
### Basic Styles Setup
The framework has already prepared basic style settings for most tags and elements, some of these settings can be easily personalized. Customizable base settings work through CSS variables, which are located at the beginning of the `root.css` file in the `:root` block, where the variables are categorized:
- System. Please do not edit or overwrite these settings — they keep other systems working. 
- General. You can personalize these variables, but note that some of these variables can be the basis for variable elements, or specific tags.  
- Body. Variables for customizing the body tag.
- Selection. Variables for customizing the selection on the website.
- Placeholder. Variables for customizing the placeholder, including the placeholder of all custom elements.
- Headers. Variables for customizing headers.

Don't forget to use already prepared and your own variables from `:root` in all styles, this is important. 

You can delete non-system variables, but we recommend doing this only if you specifically understand why you need it, and you are sure that the deleted variables are not used, and will not be used in the styles of the elements you need. In general, we don't recommend doing this.

### Variables Naming
Common variables have the following structure: `object of application` + `property name` + `property value` (`--text-color-black`, `--border-color-white`). Variables for styling specific elements have the following structure: `element` + `object of application` + `property name` (`--body-text-color`, `--h4-font-size`). You can see illustrative examples in the `root:` section.

### Containers
Can be used as a `<container>` element, or as a `<test container></test>` attribute. A block element, usually used as a parent for the main content. Depending on the size of the browser window, it adapts its `max-width` property, while centering itself along the horizontal axis. Thus, this element will be especially useful when developing a responsive site with centered main content. Breakpoints are the same as in Bootstrap.
```
<test id="header"></test>

<container>
    <test id="content"></test>
</container>

<test id="footer"></test>
```

### Grid
In general, the same as in Bootstrap, only instead of the word `col`, the letter `c`. Control points:
- `emptiness` — default
- `s` — mobile phones and above
- `m` — tablets and higher
- `l` — laptop screens and above
- `x` — external monitors and above

Write the sizes as attributes. Use sized elements as containers, don't style them. We recommend that you follow the same order of writing breakpoints as in the list above.

For those who are not familiar with this system, imagine that your screen is divided into 12 equal parts horizontally. When developing a site, you can fill in a maximum of 12 parts. Thus, to give an element a 12-part size, you need to write `c-12`. For example to specify a 4-part size, you need to write `c-4` and so on. If you want to place several blocks side by side, the sum of their sizes (parts) should not be more than 12. For example on your website can be navigation on the left with the size `c-4` and the main content on the right with the size `c-8`, so you fill in everything accessible place. If your site is responsive, then on mobile you might want to do both navigation and main content in 12-part sizes. It's very simple to do this, for navigation you set the size `c-12 c-l-4` and for the main content `c-12 c-l-8`. Thus, by default, both elements will occupy all 12 parts, but when the screen size reaches the point `l (laptop screens and above)`, the size of the navigation will be 4 parts, and the size of the main content will be 8 parts.
```
<test id="header" c-12></test>

<container>
    <test id="navigation" c-12 c-l-4></test>
    <test id="content" c-12 c-l-8></test>
</container>

<test id="footer" c-12></test>
```

### Flexbox
This framework assumes that you are developing an interface using Flexbox technology. In order to set `flex-direction` you can use `<row></row>`, `<column></column>` or the `row` and `column` attributes. Further down the documentation, you will find all the available attributes.

```
<row>
    <column>
        <test>1</test>
        <test>2</test>
        <test>3</test>
    </column>

    <column>
        <test>1</test>
        <test>2</test>
        <test>3</test>
    </column>

    <column>
        <test>1</test>
        <test>2</test>
        <test>3</test>
    </column>
</row>
```

### Sub-containers (NOT RELEVANT ANYMORE, USE COLUMN-GAP, OR ROW-GAP)
This is a unique feature fully developed by us. This system solves the problem with the grid, when you need to fill all the available space on the screen, but at the same time set specific spaces between the blocks. Unlike Bootstrap, our outer spaces is implemented using `margins`, not `padding`, and can be set to a specific value. To understand how the system works, see the simple examples below. The use of a grid (`с-12`, ...) is essential for this system to work.

Note that for single nesting `<set>` element should have `single` attribute. There will be a 60px space between the 2 black boxes:
```
<set c-12 single style="--spaces: 60px"><in row>
    <div c-6 black></div>
    <div c-6 black></div>
</in></set>
```

There will be a 40px space between the 3 black boxes:
```
<set c-12 single style="--spaces: 40px"><in row>
    <div c-6 black></div>
    <div c-3 black></div>
    <div c-3 black></div>
</in></set>
```

Try to guess what will happen here:
```
<set c-12 style="--spaces: 40px"><in row>
    <div c-2 black></div>

    <set c-8 style="--spaces: 20px"><in row>
        <div c-6 red></div>
        <div c-6 red></div>
    </in></set>

    <div c-2 black></div>
</in></set>
```

The system is not limited by nesting:
```
<set c-12 style="--spaces: 10%"><in row>
    <div c-8 black></div>

    <set c-4 style="--spaces: 20px"><in row>
        <div c-2 yellow></div>

        <set c-8 style="--spaces: 10px"><in row>
            <div c-4 red></div>
            <div c-4 red></div>
            <div c-4 red></div>
        </in></set>

        <div c-2 yellow></div>
    </in></set>
</in></set>
```

Note that the block size (for example, `c-12`), as well as the space (for example, `10%`) between direct children (not counting the `<in></in>` element), is specified in the `<set></set>` element, therefore, this element can't be styled, not only because it will break the work of this system, but also because, as you remember, you better not style the container blocks setting the size. 

As you can see, the first element inside `<set></set>` is the `<in></in>` element — this is a required construct, but you can style and work with the `<in></in>` element as you wish, just make sure it has a `row` attribute, since the spaces system works only in the horizontal plane. 

### Adaptive element
In the table below you will find a description of attributes such as `row[-s/m/l/x]` and `column[-s/m/l/x]`. For structure-changing attributes that are interchanged via breakpoint rules, we recommend using the `adaptive` element. 

```
<set c-12 single style="--spaces: 40px"><in row>
    <adaptive c-6 column row-m center-x>
        <div red></div>
        <div black></div>
    </adaptive>

    <adaptive c-6 column row-m center-x>
        <div red></div>
        <div black></div>
    </adaptive>
</in></set>
```

### Elements & Attributes
| Elements | Description | Breakpoints | Examples |
| ------ | ------ | ------ | ------ |
| `flex` | setting `display: flex` | No | `<flex></flex>` |
| `inline` | setting `display: inline-flex` | No | `<inline></inline>` |
| `row` | setting `display: flex` and `flex-direction: row` | No | `<row></row>` |
| `column` | setting `display: flex` and `flex-direction: column` | No | `<column></column>` |
| `strong`, `bold` | setting bold font via `font-weight: 600` | No | `<strong></strong>` |

| Attributes | Description | Breakpoints | Examples |
| ------ | ------ | ------ | ------ |
| `flex[-s/m/l/x]` | setting `display: flex` | Yes | `flex` |
| `inline[-s/m/l/x]` | setting `display: inline-flex` | Yes | `inline-m` |
| `row[-s/m/l/x]` | setting `display: flex` and `flex-direction: row` | Yes | `row-m` |
| `column[-s/m/l/x]` | setting `display: flex` and `flex-direction: column` | Yes | `column` |
| `onclick`, `ondblclick` | setting `cursor: pointer` | No | `onclick` |
| `locked`, `disabled` | disabling interaction with an element via `pointer-events: none` | No | `locked` |
| `user-select-all` | setting `user-select: all` | No | `user-select-all` |
| `user-select-none` | disabling the ability to select text via `user-select: none` | No | `user-select-none` |
| `overflow-[hidden/visible/scroll]` | setting `overflow` | No | `overflow-hidden` |
| `position-[relative/absolute/fixed/sticky]` | setting `position` | No | `position-relative` |
| `invisible` | setting `visibility: hidden` | No | `invisible` |
| `height-[auto/25/50/75/100/200]` | setting `height` (value in percentages) | No | `height-50` |
| `min-height-[none/100]` | setting `min-height` (value in percentages) | No | `min-height-100` |
| `max-height-[none/100]` | setting `max-height` (value in percentages) | No | `max-height-100` |
| `width-[auto/25/50/75/100/200]` | setting `width` (value in percentages) | No | `width-50` |
| `min-width-[none/100]` | setting `min-width` (value in percentages) | No | `min-width-100` |
| `max-width-[none/100]` | setting `max-width` (value in percentages) | No | `max-width-100` |
| `[top/bottom/left/right]-[auto/0/1/2/3/4]` | setting `position: relative` and `[top/bottom/left/right]` (value in pixels) | No | `top-1` |
| `[top/bottom/left/right]-[auto/0/5/10/15/20/30/40/50/60/70/80]` | setting `margin-[top/bottom/left/right]` (value in pixels) | No | `bottom-20` |
| `margin-[top/bottom/left/right]-[auto/0/5/10/15/20/30/40/50/60/70/80]` | setting `margin-[top/bottom/left/right]` (value in pixels) | No | `margin-left-20` |
| `margin-y-[auto/0/1/2/3/4/5/10/15/20/30/40/50/60/70/80]` | setting `margin-top` and `margin-bottom` (value in pixels) | No | `margin-y-5` |
| `margin-x-[auto/0/1/2/3/4/5/10/15/20/30/40/50/60/70/80]` | setting `margin-left` and `margin-right` (value in pixels) | No | `margin-x-10` |
| `padding-[0/1/2/3/4/5/10/15/20/30/40/50/60/70/80]` | setting `padding` (value in pixels) | No | `padding-10` |
| `padding-[top/bottom/left/right]-[0/1/2/3/4/5/10/15/20/30/40/50/60/70/80]` | setting `padding-[top/bottom/left/right]` (value in pixels) | No | `padding-top-80` |
| `padding-y-[0/1/2/3/4/5/10/15/20/30/40/50/60/70/80]` | setting `padding-top` and `padding-bottom` (value in pixels) | No | `padding-y-5` |
| `padding-x-[0/1/2/3/4/5/10/15/20/30/40/50/60/70/80]` | setting `padding-left` and `padding-right` (value in pixels) | No | `padding-x-10` |
| `hidden` | setting `display: none` | No | `hidden` |
| `hidden-mobile` | setting `display: none` for mobile devices only | No | `hidden-mobile` |
| `hidden-less-[s/m/l/x]` | setting `display: none` depending on the window size | Yes | `hidden-less-s` |
| `hidden-more-[s/m/l/x]` | setting `display: none` depending on the window size | Yes | `hidden-more-m` |
| `text-align[-s/m/l/x]-[left/right/center]` | setting alignment of the text | Yes | `text-align-center` |
| `top[-s/m/l/x]` | top alignment via `margin-bottom: auto`, placed on a child with a `flex` parent | Yes | `top-s` |
| `bottom[-s/m/l/x]` | bottom alignment via `margin-top: auto`, placed on a child with a `flex` parent | Yes | `bottom` |
| `left[-s/m/l/x]` | left alignment via `margin-right: auto`, placed on a child with a `flex` parent | Yes | `left-m` |
| `right[-s/m/l/x]` | right alignment via `margin-left: auto`, placed on a child with a `flex` parent | Yes | `right` |
| `wrap[-s/m/l/x]` | setting `flex-wrap: wrap`, placed on a parent and applied to children | Yes | `wrap` |
| `wrap-reverse[-s/m/l/x]` | setting `flex-wrap: wrap-reverse`, placed on a parent and applied to children | Yes | `wrap-reverse-m` |
| `no-wrap[-s/m/l/x]` | setting `flex-wrap: nowrap`, placed on a parent and applied to children | Yes | `no-wrap-s` |
| `justify-content[-s/m/l/x]-[start/end/center/between/around/evenly]` | setting `justify-content` | Yes | `justify-content-evenly` |
| `align-items[-s/m/l/x]-[start/end/center/baseline/stretch]` | setting `align-items` | Yes | `align-items-baseline` |
| `align-self[-s/m/l/x]-[start/end/center/baseline/stretch]` | setting `align-self` | Yes | `align-self-m-baseline` |
| `align-content[-s/m/l/x]-[start/end/center/around/stretch]` | setting `align-content` | Yes | `align-content-m-start` |
| `flex-grow[-s/m/l/x]-[0/1]` | setting `flex-grow` | Yes | `flex-grow-s-0` |
| `flex-shrink[-s/m/l/x]-[0/1]` | setting `flex-shrink` | Yes | `flex-shrink-1` |
| `fill[-s/m/l/x]` | setting `flex-grow: 1`, `flex-shrink: 1`, `flex-basis: auto` | Yes | `fill` |
| `fixed[-s/m/l/x]` | setting `flex-grow: 0`, `flex-shrink: 0` | Yes | `fixed` |
| `order-first[-s/m/l/x]` | setting `order: -1` | Yes | `order-first-l` |
| `order-[1/2/3/4/5/6/7/8/9][-s/m/l/x]` | setting `order` | Yes | `order-6-s` |
| `order-last[-s/m/l/x]` | setting `order: 666` | Yes | `order-last` |
| `center-x` | horizontal alignment, placed on a `flex` parent and applied to children. Use with `row[-s/m/l/x]` or `column[-s/m/l/x]`, or with `<row>` or `<column>` elements | Auto | `center-x` |
| `center-y` | vertical alignment, placed on a `flex` parent and applied to children. Use with `row[-s/m/l/x]` or `column[-s/m/l/x]`, or with `<row>` or `<column>` elements | Auto | `center-y` |
| `center` | horizontal and vertical alignment, placed on a `flex` parent and applied to children. Use with `row[-s/m/l/x]` or `column[-s/m/l/x]`, or with `<row>` or `<column>` elements | Auto | `center` |
| `lowercase`, `capitalize`, `uppercase` | changing the case of the text | No | `lowercase`, `capitalize` |
| `font-size-[s/m/l/4/3/2/1]` | sets the text size corresponding to the variable of the same name  | No | `font-size-m`, `font-size-3` |
| `font-weight-[100/200/300/400/500/600/700/800/900]` | changing `font-weight` of the text | No | `font-weight-300` |
| `strong`, `bold` | setting bold font via `font-weight: 600` | No | `strong`, `bold` |
| `letter-spacing-[1/2/3]` | setting letter spacing (value in pixels) | No | `letter-spacing-1` |
| `text-overflow` | replacing text that does not fit into `...` | No | `text-overflow` |
| `text-strike` | strikethrough text | No | `text-strike` |
| `opacity-[0/5/10/15/20/25/30/33/35/40/45/50/55/60/65/66/70/75/80/85/90/95/100]` | setting `opacity` | No | `opacity-80` |

### Breakpoints
At the end of the `root.css`, you can find some empty media rules that you can use for custom styles.

### Zero Margins Control
Control zero margin for desired elements in sequence. Using any of the `margin` attributes (`margin-`, `margin-top-`, `right-`, `margin-y` and others from the table above) add a keyword before the value 0 to highlight the desired sequence element. Items must be at the same nesting level. 

| Keyword | Description | Example
| ------ | ------ | ------ |
| `ft` | setting `first-of-type` | `margin-ft-0` |
| `lt` | setting `last-of-type `| `margin-x-lt-0` |
| `2n` | setting `nth-of-type(2n)` | `top-2n-0` |
| `3n` | setting `nth-of-type(3n)` | `bottom-3n-0` |
| `4n` | setting `nth-of-type(4n)` | `left-4n-0` |
| `5n` | setting `nth-of-type(5n)` | `margin-right-5n-0` |
| `6n` | setting `nth-of-type(6n)` | `margin-y-6n-0` |

```
<div flex>
    <div right-20 right-2n-0>Content</div>
    <div right-20 right-2n-0>Content</div> <!-- Here will be margin-right: 0 -->
    <div right-20 right-2n-0>Content</div>
</div>
```

# Our approach
In CSS and JS, in 99% of cases, we use the character `'` to select a string, when we need a quotation mark in a quotation mark, we write `'/` 

---

All CSS styles are written on a new line with the closing character `;`

---

A blank line must always be skipped after a multi-line block of code (in CSS, JS, and HTML), unless a single line of code (usually assigning or returning a variable) is directly linked to a multi-line block of code (usually processing a variable)

Wrong:
```
return this.each(function() {
    $.attr(this, nameNew, $.attr(this, name));
    $.removeAttr(this, name);
    if (removeData !== false)
        $.removeData(this, name.replace('data-', ''));
});
```

Undesirable:
```
var selected = [];

$('#' + element + ' [select-item].active').each(function() {
    if ($(this).attr('value') != '')
        selected.push($(this).attr('value'));  
});

return selected;
```

Correctly: 
```
return this.each(function() {
    $.attr(this, nameNew, $.attr(this, name));
    $.removeAttr(this, name);

    if (removeData !== false)
        $.removeData(this, name.replace('data-', ''));
});

var selected = [];
$('#' + element + ' [select-item].active').each(function() {
    if ($(this).attr('value') != '')
        selected.push($(this).attr('value'));  
});
return selected;
```

---

Don't use `!important` when we can do without it

---

For third-party JS and CSS add-ons, we do not change the source code, but overwrite it

---

We are guided by logic and some kind of symmetry when developing a design. If the middle padding is `50px`, then the big one will be `100px`, not `103px`

---

To catch an event in jQuery, we use the `$(document).on('event', 'element', callback)` construct

---

Focus on spaces. Correctly:

`function() {`

`'test' + myVar + 'text'`

`if ((option == 'test1') || (option == 'test2'))`

---

If after the condition only one function / line should be executed (at this nesting level), then we don't write `{}`

```
if ((option == 'test1') || (option == 'test2'))
    if (myVar == true)
        return 'example1';
    else
        return 'example2';
```

---

Use the ternary operator whenever possible. The conditions in the ternary operator are written between `()`

We will write the example above like this:
```
if ((option == 'test1') || (option == 'test2'))
    return (myVar == true) ? 'example1' : 'example2';
```
---

Correctly:

`.test(function() { return !this.value })`

---

Correctly:

`.test(() => 'no this here')`

---

Nesting is created through TAB (TAB size — 4 spaces)

---

We use only the most popular abbreviations like: `var`, `attr`, `ini`, `lang`, `img`, `src`. We don't use:  `btn`, `elem`, `elems`, `err`, `e`, `x`, `y`, `chbx` etc.

---

Don't add extra commas for the last elements (in arrays, objects), even if it works

---

We try to wrap the line only by logical blocks and only where it's really necessary

Wrong:
```
const elems = document.querySelectorAll(
    '#test1, #test2, #test3'
);

headers: {
    'test1': 'test1',
    'test2': document
        .querySelector('#test')
        .getAttribute('content'),
}

if (toDeactivate.length == 0) return;
```

Correctly:
```
const elements = document.querySelectorAll('#test1, #test2, #test3');

headers: {
    'test1': 'test1',
    'test2': document.querySelector('#test').getAttribute('content')
}

if (toDeactivate.length == 0)
    return;
```

---

We use arrow functions where possible. For arrow functions, input variables are always written between `()`

Wrong:
```
.then(function(response) {
    if (response.status === 200) {
        location.reload();
    }
    return false;
});
```

Correctly:
```
.then((response) => (response.status == 200) ? location.reload() : false);
```

---

We write 2-character words in all functional names (variables, classes, functions, parameters, and so on) in capital letters. Instead of `customerId`, we write `customerID`

---

After `,` we always write a space

# What does our team use additionally? 
- [jquery](https://jquery.com)
- [jquery ui](https://jqueryui.com)
- [jquery ui multidatespicker](https://dubrox.github.io/Multiple-Dates-Picker-for-jQuery-UI)
- [jquery popupoverlay](https://dev.vast.com/jquery-popup-overlay)
- [jquery flexdatalist](http://projects.sergiodinislopes.pt/flexdatalist)
- [jquery nice select](https://jqueryniceselect.hernansartorio.com)
- [wow](https://wowjs.uk)
- [animate](https://animate.style)
- [fontawesome](https://fontawesome.com)
